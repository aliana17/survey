from django.shortcuts import render, redirect, reverse
from .models import Question, Customers, Answers
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
import uuid, random

# Create your views here.

def home(request):
    return render(request, 'survey/index.html', context={})

@csrf_exempt
def handler(request,cust_id,id):
    ques_list = [question for question in Question.objects.all()] #will call the database only one time
    n = len(ques_list)
    if(n<=0):
        raise Http404("Please insert questions for survey")
    if(request.method == 'GET'):
        print(id)
        if(id<=0):
            id = 0
        else:
            id = id-1
    elif(request.method == 'POST'):
        if(id >= n-1):
            return render(request, 'survey/form.html', context={"question":ques_list[id], "cust_id":cust_id, "islast": True})
        if(id<n):
            #save the answer to database
            input = request.POST['ans'+str(id)]
            print(input)
            c= Customers.objects.get(cust_id=cust_id)
            q = Question.objects.get(pk=id)
            resp = Answers(customer=c, ques=q, text=input)
            resp.save()
            id = id+1
    return render(request, 'survey/form.html', context={"question":ques_list[id], "cust_id":cust_id})

@csrf_exempt
def start(request):
    #cust_id = str(uuid.uuid4())
    cust_id = 13
    c = Customers(cust_id=cust_id)
    c.save()
    return redirect(f'questions/{cust_id}/0')
    #return reverse('survey:handler', kwargs={'id':0, 'cust_id':cust_id})

def close(request):
    return render(request,'survey/close.html')

